/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Windows10
 */
public class TestAnimal {
    public static void main(String[] args){
        Animal animal=new Animal("Ani","White",0);
        animal.speak();
        animal.walk();
        
        Dog dang =new Dog("Dang","Blac&White");
        dang.speak();
        dang.walk();
        
        Cat zero = new Cat("Zero","Orange");
        zero.speak();
        zero.walk();
        
        Duck zom= new Duck("Zom","Orange");
        zom.speak();
        zom.walk();
        zom.fly();
        
        System.out.println("Zom is Animal: "+(zom instanceof Animal));
        System.out.println("Zom is Duck: "+(zom instanceof Duck));
        System.out.println("Zom is Cat: "+(zom instanceof Object));
    }
}
